package aaron.util.geom;

abstract class AbstractShape implements Shape {
    private String name;

    // METHODS
    abstract boolean amPointy();

    // GETTERS & SETTERS
    public String getName() {
        return name;
    }

    public void setName(String n) {
        name = n;
    }
}
