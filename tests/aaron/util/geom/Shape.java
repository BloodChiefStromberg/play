package aaron.util.geom;

public interface Shape {
    public float area();
    public int perimeter();
}
