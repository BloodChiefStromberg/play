package aaron.util.geom;

import aaron.util.geom.AbstractShape;

public class Square extends AbstractShape {
    private int length;
    private int width;

    // DELETE THIS
    public Square(int l, int w) {
        length = l;
        width = w;
    }

    // OVERRIDES
    @Override
    public float area() {
        return length * width;
    }

    @Override
    public int perimeter() {
        return 2 * (length + width);
    }

    @Override
    public boolean amPointy() {
        return true;
    }
}
