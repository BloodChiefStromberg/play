package aaron.util.geom;

import aaron.util.geom.Shape;
import aaron.util.geom.SimpleShape;

public class ShapeFactory {

    public static Shape getShape(String name) {
        if (name SimpleShape.getTypes().contains(name)) {
            return SimpleShape.build(name);
        }
